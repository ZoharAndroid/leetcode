# LeetCode题目 ![](https://img.shields.io/badge/build-passing-brightgreen.svg) ![](https://img.shields.io/badge/language-Java-brightgreen.svg)


> 1. https://leetcode-cn.com/problemset/all/ 
> 2. 对应的序号就是题目的标号
> 3. 有时间就会刷题然后更新一下，记录进度
> 4. Java语言
> 5. 代码全部都是验证通过的
> 6. readme没有及时更新或者链接有问题可以直接点击源代码进行查看


序号|名称|等级
:-----:|:------------------------:|:-:
[434][434]|[字符串中的单词数][434]|简单
[429][429]|[N叉树的层序遍历][429]|简单
[427][427]|[建立四叉树][427]|简单
[415][415]|[字符串相加][415]|简单
[217][217]||简单
[206][206]| |简单
[205][205]| |简单
[204][204]| |简单
[203][203]| |简单
[202][202]| |简单
[198][198]| |简单
[191][191]| |简单
[190][190]| |简单
[189][189]| |简单
[172][172]| |简单
[171][171]| |简单
[169][169]| |简单
[167][167]| |简单
[168][168]| |简单
[166][166]| |简单
[155][155]| |简单

[434]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_434.java 
[429]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_429.java 
[155]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_155.java 
[166]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_166.java
[168]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_168.java
[167]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_167.java
[169]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_169.java
[171]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_171.java
[172]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_172.java
[189]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_189.java
[190]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_190.java
[191]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_191.java
[198]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_198.java
[202]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_202.java
[203]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_203.java
[204]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_204.java
[205]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_205.java
[206]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_206.java
[217]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_217.java
[415]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_415.java
[427]:https://gitlab.com/ZoharAndroid/leetcode/blob/master/_427.java



